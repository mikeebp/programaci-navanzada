#include <pthread.h>
#include <stdio.h>

void * hello(void *id)
{
  //unsigned long id = (unsigned long) arg;
  printf("Hello World %lu\n", *(long unsigned*)id );
  return NULL;
}

#define NUM_THREADS 10

int main(){
    pthread_t threads[NUM_THREADS];
    long unsigned i;
    for(i = 0; i< NUM_THREADS;i++){
      pthread_create(&threads[i], NULL, hello, (void *)&i);
    }

    while(1){}
  return 0;
}
