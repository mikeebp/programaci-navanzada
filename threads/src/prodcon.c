#include <pthread.h>
#include <stdio.h>
#include <semaphore.h>

#define BUFSIZE 10
#define MAXINT 1000
unsigned int buf[BUFSIZE];

sem_t available;
sem_t ready;
sem_t ptotal_mutex;
sem_t ppos_mutex;
unsigned int ptotal;
unsigned int ppos;
void * producer(void *id)
{
  unsigned int pos = 0;
  unsigned int value = 0;
  printf("I am the producer \n");
  while(1)
  {
    sem_wait(&ptotal_mutex);
    value = ptotal;
    if(ptotal++ >= MAXINT)
    {
      sem_post(&ptotal_mutex);
      break;
    }
    sem_post(&ptotal_mutex);

    sem_wait(&available);
    sem_wait(&ppos_mutex);
    pos = pspos; //region critica
    ppos = (++ppos)%BUFSIZE; //region crítica
    sem_post(&ppos_cmutex);

    buf[pos] = MAXINT - value;
    sem_post(&ready);
  }
  printf("done producing\n" );
  return NULL;
}

unsigned int cpos;
unsigned int ctotal;
sem_t cmutex;
sem_t ctotal_mutex;
void * consumer(void *id)
{
  unsigned int pos;
  printf("I am the consumer \n");

  while(1)
  {
    sem_wait(&ctotal_mutex);
    if(++ctotal >= MAXINT)
    {
      break;
    }
    sem_post(&ctotal_mutex);

    sem_wait(&ready);
    sem_wait(&cmutex);
    pos = cpos; //region critica
    cpos = (++cpos)%BUFSIZE; //region crítica
    sem_post(&cmutex);

    printf("value is %u \n",buf[pos]);
    sem_post(&available);

  }
  sem_post(&ctotal_mutex);

  printf("done consuming\n" );
  return NULL;
}

#define NUM_CONSUMERS 4
#define NUM_PRODUCERS 4
#define NUM_THREADS (NUM_PRODUCERS + NUM_CONSUMERS)
int main(){
    pthread_t threads[NUM_THREADS];
    long unsigned i;
    cpos = 0;
    //semaforo, tipo de semafaor, [0 es hilo], tamaño
    sem_init(&available,0,BUFSIZE);
    sem_init(&ready,0,0);
    sem_init(&cmutex,0,1);

    ppos = 0;
    sem_init(&ppos_mutex,0,1);

    sem_init(&ctotal_mutex,0,1);s

    for (i = 0; i < NUM_PRODUCERS; i++) {
      pthread_create(&threads[i], NULL, producer, NULL);
    }

    // i continue after NUM_PRODUCERS
    for (; i < NUM_CONSUMERS; i++) {
      pthread_create(&threads[i], NULL, consumer, NULL);
    }

    for(i = 0; i< NUM_THREADS;i++){
      pthread_join(threads[i], NULL); //espera que los hilos terminen
    }

    return 0;
}
