/*
*   Conversions
*   Miguel Ángel Basilio de la Paz
*   A01228213
*   05 de septiembre de 2016
*   Descripción: Conversiones entre los sistemas métrico inglés e internacinal.
*/

#include <stdio.h>
#include <stdlib.h>

float kg_to_lb(float number, int cnt){
  float new_value = number * 2.20462;
  printf("%d ", cnt);
  printf("%0.4lf", new_value);
  printf("%s\n", " lb");
  return new_value;
}

float lb_to_kg(float number, int cnt){
  float new_value = number * 0.453592;
  printf("%d ", cnt);
  printf("%0.4lf", new_value);
  printf("%s\n", " kg");
  return new_value;
}

float l_to_gal(float number, int cnt){
  float new_value = number * 0.264172;
  printf("%d ", cnt);
  printf("%0.4lf", new_value);
  printf("%s\n", " gal");
  return new_value;
}

float gal_to_l(float number, int cnt){
  float new_value = number * 3.78541;
  printf("%d ", cnt);
  printf("%0.4lf", new_value);
  printf("%s\n", " l");
  return new_value;
}

int operation_check(double number, char *unit, int cnt){

  if (!strcmp(unit,"kg")) {
    kg_to_lb(number, cnt);
  } else
  if (!strcmp(unit,"lb")) {
    lb_to_kg(number, cnt);
  } else
  if (!strcmp(unit,"l")) {
    l_to_gal(number, cnt);
  } else
  if (!strcmp(unit,"gal")) {
    gal_to_l(number, cnt);
  }

  return 0;

}

int read_input(){
  int size;
  char unit[5];
  double number;
  FILE * fp;

  fp = fopen ("input.txt", "r");

  fscanf(fp, "%d", &size);
  int cnt = 0;
  while(fscanf(fp,"%lf %s \n",&number,unit)==2)
  {
    cnt++;
    operation_check(number,unit,cnt);
  }

  fclose(fp);

  return 0;

}

int main(int argc, char **argv) {

  read_input();

  return 0;
}
