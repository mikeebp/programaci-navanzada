Conversions
==============

Miguel Ángel Basilio de la Paz
A01228213
--------------

**Descripción**
El objetivo de este programa es convetir entre el sistema métrico inglés y el
internacional.

A través de un input.txt se enlistan los argumentos que serán procesados
por el programa, de la siguiente forma:
5
1 kg
2 l
7 lb
3.5 gal
0 l

*Unidades*
l --> litros
gal --> galones
lb --> libra
kg --> kilogramo

El resultado será mostrado en consola de la siguiente manera, usando el mismo ejemplo anterior:
1 2.2046 lb
2 0.5284 gal
3 3.1752 kg
4 13.2489 l
5 0.0000 gal

Link al ejercicio de ACM:
http://web.archive.org/web/20150315093210/http://acm.tju.edu.cn/toj/showp3005.html
