#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned debug;

// Donde sea que encuentre printdebug, C irá y reemplazará el texto literal a esta línea de código
#define printdebug(msg) if(debug) printf("%s\n",(msg))

int main()
{
  char *value;
  char message[100];
  FILE *file;
  int c;
  long whence;

  debug = 0;
  // Get debug env info
  value = getenv("DEBUG");

  if(value && strcmp(value,"1") == 0)
  {
      debug = -1;
  }

  file = fopen("io.c","r");
  if(file == NULL)
  {
    sprintf(message,"Error when opening the file.");
    printdebug(message);
    return -1;
  }

  whence = ftell(file);
  while(1)
  {
    c = fgetc(file);
    if(c == EOF){
      whence = ftell(file) - whence;
      fseek(file, SEEK_END, -1*(whence));
    }
    printf("%c",c);
  }
  fclose(file);


  return 0;

}
