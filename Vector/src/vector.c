#include <stdlib.h>
#include <string.h>
#include "vector.h"

void VectorNew(Vector *v, int elemSize, int initialAllocation)
{
  v->elemSize = elemSize;
  v->initialAllocation = initialAllocation;
  v->elems = malloc(v->elemSize * v->initialAllocation);
  v->nelems = 0;
  return;
}

void VectorAppend(Vector *v, const void *elemAddr)
{
  if(v->nelems == v->initialAllocation)
  {
    v->initialAllocation *= 2;
    v->elems = realloc(v->elems,v->initialAllocation * v->elemSize);
  }
  memcpy((char *)v->elems + v->nelems * v->elemSize,elemAddr,v->elemSize);
  v->nelems += 1;
  return;
}

int VectorLength(const Vector *v)
{
  return v->nelems;
}

void VectorReplace(Vector *v, const void *elemAddr, int position)
{
  if (position >= 0 && position < v->initialAllocation)
  {
    memcpy((char *)v->elems + v->nelems * position, elemAddr, v->elemSize);
  }

  return;
}

void VectorNth(const Vector *v, void *elemAddr, int position)
{
  if (position >= 0 && position < v->initialAllocation)
  {
    memcpy(elemAddr, (char *)v->elems + v->nelems * position, v->elemSize);
  }
  return;
}

void VectorInsert(Vector *v, const void *elemAddr, int position)
{
  if(position >= 0 && position < v-> initialAllocation){

	int elemRight = v->nelems - position + 1;


	memcpy((char *)v->elems + v->nelems * position, elemAddr, v->elemSize);
  }
  return;
}

void VectorMap(Vector *v, VectorMapFunction mapfn, void *auxData)
{
  void *elemAddr;
  int i;

  for(i = 0; i < v-> nelems; i++)
  {
    elemAddr = (char *)v->elems + i*v->elemSize;
    mapfn(elemAddr, auxData);
  }

  return;
}

void VectorSort(Vector *v, VectorCompareFunction comparefn)
{
  qsort(v->elems,v->nelems,v->elemSize,comparefn);
  return;
}
