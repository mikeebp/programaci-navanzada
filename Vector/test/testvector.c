#include <stdio.h>
#include <string.h>
#include "../src/vector.h"
#include "utest.h"

//Global variable
char msg[100];
Vector letters;

static char * test_VectorNew()
{
  int elemSize = sizeof(char);
  int initialAllocation = 10;

  VectorNew(&letters,elemSize,initialAllocation);
  sprintf(msg,"Elemsize must be %d",elemSize);
  assert_test(msg,letters.elemSize == elemSize);
  sprintf(msg,"initialAllocation must be %d",initialAllocation);
  assert_test(msg,letters.initialAllocation == initialAllocation);
  sprintf(msg,"elems must have enough space for %d",initialAllocation * elemSize);
  assert_test(msg,malloc_usable_size(letters.elems));

  return 0;
}

static char * test_VectorAppend()
{
  char c;
  int i=0;
  char cletters[100];
  for(c='a';c<='z';c++,i++)
  {
    VectorAppend(&letters,&c);
    cletters[i] = c;
  }

  cletters[i] = '\0';

  sprintf(msg,"Vector must content %s \n",cletters);
  assert_test(msg,strncmp(letters.elems,cletters,i-1) == 0);
  return 0;
}

static char * test_VectorLength()
{
  int nelems = VectorLength(&letters);

  sprintf(msg,"nelems must be equal to %d \n", nelems);
  assert_test(msg,letters.nelems == nelems);

  return 0;
}

static char * test_VectorReplace()
{
  char c = 'z';
  int position = 0;

  sprintf(msg,"position cannot be less than 0 or greater than vector length -1");
  assert_test(msg,position < letters.initialAllocation || position >= 0);

  VectorReplace(&letters,&c,position);

  char * string_letters = letters.elems;
  char c_letters = string_letters[position];

  sprintf(msg,"c_letters must be equal to %c \n", c);
  assert_test(msg, c == c_letters);

  return 0;
}

static char * test_VectorNth()
{
  char new_char='0';
  int position = 0;

  sprintf(msg,"position cannot be less than 0 or greater than vector length -1");
  assert_test(msg,position < letters.initialAllocation || position >= 0);

  VectorNth(&letters,&new_char,position);

  char * string_letters = letters.elems;
  char c_letters = string_letters[position];

  sprintf(msg,"value for char c must be equal to %c", c_letters);
  assert_test(msg,new_char == c_letters);

  return 0;
}

static char * test_VectorInsert()
{
  char the_char = 'w';
  int position = 0;

  sprintf(msg,"position cannot be less than 0 or greater than vector length -1");
  assert_test(msg,position < letters.initialAllocation || position >= 0);

  char * first_array = letters.elems;
  VectorInsert(&letters,&the_char,position);
  char * second_array = letters.elems;

  int i; // Index del arreglo antes de VectorInsert
  int j; // Index del arreglo después de VectorInsert
  int flag = 1; // Debe permanecer con valor 1 para que sea correcto

  printf("%d\n", flag);

  for (i = 0, j = 0; j < letters.initialAllocation; i++, j++)
  {
    if (j == position)
    {
      if(second_array[position] != the_char)
      {
        // No se insertó nuestro char (the_char) en la posición asignada (position)
        flag = 0;
      }
      // No avanzamos el index del primer arreglo para que se compare correctamente con el segundo arreglo
      // Esto debido a que se insertó en medio un valor y las posiciones originales ya no son iguales
      i--;
      continue;
    }
    if (first_array[i] != second_array[j])
    {
      // Si no son iguales los valores, significa que no se desplazaron correctamente después de la inserción
      flag = 0;
    }
  }

  printf("%d\n", flag);

  sprintf(msg,"flag must contain value '1'");
  assert_test(msg, flag == 1);

  return 0;
}

void accumulative(void *elem, void *total)
{
  int *a, *b;
  a = (int *)elem;
  b = (int *)total;

  *b += *a;
}

void printInt(void *elem, void *aux)
{
  printf("%d ", *((int *)elem));
}

static char * test_VectorMap()
{
  Vector numbers;
  int sizeElem = sizeof(int);
  int iniSize = 10;
  int i;
  int result = 0;
  int total = 0;

  VectorNew(&numbers,sizeElem,iniSize);

  for (i = 0; i < 20; i++)
  {
    VectorAppend(&numbers,&i);
    result += i;
  }

  VectorMap(&numbers,accumulative,(void *)&total);

  printf(" [ ");
  VectorMap(&numbers,printInt,NULL);
  printf(" ] \n");

  sprintf(msg,"Total must be %d \n", result);
  assert_test(msg, total == result);

  return 0;

}

void printString(void *elem, void *aux)
{
  char *s = *(char **)elem;
  printf("%s ",s);
}

int cmpString(void *elem1, void *elem2)
{
  // -1 para invertir
  return strcmp(*(const char **)elem1, *(const char **)elem2);
}

static char * test_VectorSort()
{
  Vector names;
  int elemSize = sizeof(char *);
  int iniSize = 10;
  char *dir;

  VectorNew($names,elemSize,iniSize);

  dir = strdup("Miguel");
  VectorAppend($names,&dir);
  dir = strdup("Angélica");
  VectorAppend($names,&dir);
  dir = strdup("Juan");
  VectorAppend($names,&dir);

  printf(" [ ");
  VectorMap(&names,printString,NULL);
  printf(" ] ");

  VectorSort(&names,cmpString);

  printf(" [ ");
  VectorMap(&names,printString,NULL);
  printf(" ] ");

}


int main()
{
  run_test("Testing VectorNew",test_VectorNew);
  run_test("Testing VectorAppend",test_VectorAppend);
  run_test("Testing VectorLength",test_VectorLength);
  run_test("Testing VectorReplace",test_VectorReplace);
  run_test("Testing VectorNth",test_VectorNth);
  run_test("Testing VectorInsert",test_VectorInsert);
  run_test("Testing VectorMap",test_VectorMap);
  return 0;
}
