#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>

int main()
{
    int n;
    scanf("%d",&n);
    int *a = malloc(sizeof(int) * n);
    for(int a_i = 0; a_i < n; a_i++)
    {
       scanf("%d",&a[a_i]);
    }
    int singleNumber;
    int compareNumber;
    int found = 0;
    for(int i = 0; i < n; i++)
    {
       singleNumber = a[i];
       printf("Single number = %d\n", singleNumber);
       for(int j = 0; j < n; j++)
       {
           compareNumber = a[j];
           printf("Compare number = %d\n", compareNumber);
           if(singleNumber == compareNumber)
           {
               found++;
           }
       }
       printf("FOUND = %d\n", found);
       if(found != 2)
       {
           printf("RESULTADO = %d\n", singleNumber);
       }
       found = 0;
    }
    return 0;
}
