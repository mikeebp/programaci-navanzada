/*
*   Fibonacci con uso de memoria
*   Miguel Ángel Basilio de la Paz
*   A01228213
*   22 de agosto de 2016
*   Descripción: Esta función calcula el fibonacci de un número
*   recursivamente y, además, guarda el valor en memoria para no volver
*   a calcular.
*/

#include <stdio.h>

unsigned long long int fibonacci(unsigned long long int n,
                                 unsigned long long int *memoization){

  if (memoization[n] != 0 || n == 0)
    return memoization[n];

  memoization[n] = fibonacci(n-1, memoization) + fibonacci(n-2, memoization);

  return memoization[n];

}

int main(int argc, char **argv){
  long long int memoization[100] = {0,1,0};
  printf("%llu\n", fibonacci(atoi(argv[1]), memoization));
  return 0;
}
