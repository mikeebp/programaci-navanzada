/*
*   Fibonacci
*   Miguel Ángel Basilio de la Paz
*   A01228213
*   22 de agosto de 2016
*   Descripción: Esta función calcula el fibonacci de un número recursivamente.
*/

#include <stdio.h>

unsigned long long int fibonacci(unsigned long long int num){

  if(num == 0 || num == 1){
    return num;
  } else {
    return fibonacci(num-1) + fibonacci(num-2);
  }

}

int main(int argc, char **argv){
  printf("%llu\n", fibonacci(atoi(argv[1])));
  return 0;
}
