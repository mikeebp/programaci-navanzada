#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>

int* getMinimumUniqueSum(int arr_size, char** arr, int* result_size) {
    int a, b;
    int n,i,j;
    int *res;
    double root;

    *result_size = arr_size;

    n = arr_size;
    res = (int *)malloc(sizeof(int)*arr_size);

    for(i=0; i<n; i++){
        a = atoi(arr[i]);
        j = 0;
        while(arr[i][j++] != ' ');
        b = atoi(&arr[i][j]);
        res[i] = 0;

        for (;a<=b;a++){
            root = sqrt((double)a);
            if((root - (int)root) == 0.0) res[i]++;

        }

    }

    return res;

}

int main() {
    FILE *f = fopen(getenv("OUTPUT_PATH"), "w");
    int res_size;
    int* res;

    int _arr_size = 0;
    int _arr_i;
    scanf("%d\n", &_arr_size);
    char* _arr[_arr_size];
    for(_arr_i = 0; _arr_i < _arr_size; _arr_i++) {
        char* _arr_item;
        _arr_item = (char *)malloc(512000 * sizeof(char));
        scanf("\n%[^\n]",_arr_item);

        _arr[_arr_i] = _arr_item;
    }

    res = getMinimumUniqueSum(_arr_size, _arr, &res_size);
    int res_i;
    for(res_i=0; res_i < res_size; res_i++) {

        fprintf(f, "%d\n", res[res_i]);

    }


    fclose(f);
    return 0;
}
