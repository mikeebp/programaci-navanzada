#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv){

  int sum = 0;
  int i;
  for (i = 0; i < argc; i++) {
    sum = sum + atoi(argv[i]);
  }

  printf("El resultado es %d \n", sum);

  return 0;

}
