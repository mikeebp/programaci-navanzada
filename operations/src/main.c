/*
  Student ID: A01228213.
  Author: Miguel Ángel Basilio de la Paz.
  Date: August 31, 2016.
  Description: Calculate the sum between two float numbers.
*/
#include <stdio.h>
#include <string.h>
#include "operations.h"

int main()
{
  char msg[] = "What is the operation?";
  char * operation_names[]=
  {
    "sum",
    "subtract",
    "division"
  };
//Esto es un arreglo de funciones que regresan float
  float (*operations[])(float, float) = {
    sum, subtract, division
  };
  char operation[0];
  float operator1,operator2,result;
  //Dividir la cantidad de bytes del arreglo entre los bytes que tiene un char para tener la cantidad.
  size_t nelem = sizeof(operation_names)/sizeof(char *);

  while(1)
  {
    int i;
    printf("%s\n",msg);
    scanf("%s",&operation[0]);
    tolower_string(operation);

    if(strcmp((operation),"finish")  == 0 )
      break;

    for (i = 0; i < nelem; i++) {
      if (strcmp(operation_names[i], operation) == 0) {
        break;
      }
    }
    if (i == nelem) {
      printf("Nonvalid operation: %s\n", operation);
      continue;
    };

    if(strcmp(tolower_string(operation),"sum")  == 0 )
    {
      scanf("%f %f",&operator1,&operator2);
      printf("Result: %.1f\n",(operations[0])(operator1,operator2));
    }
    else if(strcmp(tolower_string(operation),"subtract")  == 0 )
    {
      scanf("%f %f",&operator1,&operator2);
      printf("Result: %.1f\n",(operations[1])(operator1,operator2));
    }


  }

  return 0;
}
