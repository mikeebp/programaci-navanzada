/*
  Student ID: A01228213.
  Author: Miguel Ángel Basilio de la Paz.
  Date: August 31, 2016.
  Description: Calculate the sum between two float numbers.
*/
#ifndef __OPERATIONSH__
#define __OPERATIONSH__

float sum(float a, float b);
float subtract(float a, float b);
float division(float a, float b);
char * tolower_string(char *s);

#endif
