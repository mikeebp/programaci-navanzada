/*
  Student ID: A01228213.
  Author: Miguel Ángel Basilio de la Paz.
  Date: August 31, 2016.
  Description: Calculate the sum between two float numbers.
*/
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "operations.h"

float sum(float a, float b)
{
  return a+b;
}

float subtract(float a, float b)
{
  return a-b;
}

float division(float a, float b)
{
  return a/b;
}

char * tolower_string(char *s)
{
  int i;

  for (i = 0; i < strlen(s); i++)
    s[i] = tolower(s[i]);

  return s;
}
