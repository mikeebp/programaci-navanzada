/*
  Student ID: A01228213.
  Author: Miguel Ángel Basilio de la Paz.
  Date: August 31, 2016.
  Description: Calculate the sum between two float numbers.
*/
#include <stdio.h>
#include "utest.h"
#include "../src/operations.h"

//Static hace que una función sea privada solamente para el archivo donde se escribe.
//Esto evita que el código se filtre en cualquier otro lugar.
static char * test_sum()
{
  float a,b,c;

  a = 23.5;
  b = 5.0;
  c = sum(a,b); // function to test, must be only one

  assert_test("Wrong! Sum is not correct ", c == a + b);

  return 0;

}

static char * test_subtract()
{
  float a,b,c;

  a = 23.5;
  b = 5.0;
  c = subtract(a,b); // function to test, must be only one

  assert_test("Wrong! Subtract is not correct ", c == a - b);

  return 0;

}

static char * test_tolower_string()
{
  // strdup --> Asigna un espacio de memoria donde quepa el string y regresa la
  // direción del primer elemento
  char *str_test = strdup("STRING in UPPERCASE");
  char *str_expected = "string in uppercase";
  char *str_result = tolower_string(str_test);

  // strcmp --> Regresa 0 cuando ambos parámetros son iguales
  assert_test("String must be in lower case", !strcmp(str_expected, str_result));

  return 0;
}

int main ()
{
  run_test("Test sum:", test_sum);
  run_test("Test subtract:", test_subtract);
  run_test("Test to lower:", test_tolower_string);
  return 0;
}
