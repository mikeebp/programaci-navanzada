#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>

int stairsCase(int n, int *memoization)
{
    if(n < 1)
    {
        return 0;
    } 
    else if(memoization[n-1]!=0)
    {
        return memoization[n-1];
    }
    else
    {
        memoization[n-1] = stairsCase(n-1,memoization) + stairsCase(n-2,memoization) + stairsCase(n-3,memoization);
        return memoization[n-1];
    }
}

int main(){
    int s;
    scanf("%d",&s);
    int memoization[36] = {1,2,4,0};
    for(int a0 = 0; a0 < s; a0++){
        int n;
        scanf("%d",&n);
        printf("%d\n",stairsCase(n,memoization));
    }
    return 0;
}
